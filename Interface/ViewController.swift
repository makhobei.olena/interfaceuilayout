//
//  ViewController.swift
//  Interface
//
//  Created by Oleh Makhobei on 05.08.2021.
//

    import UIKit

    class ViewController: UIViewController {
       // var dataTxt : String? = nil
        
       
        @IBOutlet var dataLbl: UILabel!
        @IBOutlet var loadButton: UIButton!
        @IBOutlet var clearButton: UIButton!
        
        @IBAction func loadBttTapped(_ sender: UIButton) {
            dataLbl.text = "The fundamental building block in Auto Layout is the constraint. Constraints express rules for the layout of elements in your interface; for example, you can create a constraint that specifies an elementТs width, or its horizontal distance from another element. You add and remove constraints, or change the properties of constraints, to affect the layout of your interface.When calculating the runtime positions of elements in a user interface, the Auto Layout system considers all constraints at the same time, and sets positions in such a way that best satisfies all of the constraints.Constraint Basics.You can think of a constraint as a mathematical representation of a human-expressable statement. If youТre defining the position of a button, for example, you might want to say Уthe left edge should be 20 points from the left edge of its containing view.Ф More formally, this translates to button.left = (container.left + 20), which in turn is an expression of the form y = m*x + b, where:"

        }
        
        @IBAction func clearBttTapped(_ sender: UIButton) {
            dataLbl.text = ""
            
        }
        override func viewDidLoad() {
            super.viewDidLoad()
            dataLbl.sizeToFit()
            dataLbl.text = ""
            loadButton.layer.cornerRadius = 5
            clearButton.layer.cornerRadius = 5
            // Do any additional setup after loading the view.
        }


    }




